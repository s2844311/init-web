package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> data = new HashMap<>();

    public Quoter(){
        data.put("1", 10.0);
        data.put("2", 45.0);
        data.put("3", 20.0);
        data.put("4", 35.0);
        data.put("5", 50.0);
    }
    public double getBookPrice(String isbn){
        return data.get(isbn) == null ? 0.0 : data.get(isbn);
    }
}
